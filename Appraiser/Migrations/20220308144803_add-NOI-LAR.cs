﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Appraiser.Migrations
{
    public partial class addNOILAR : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "NetOperatingIncome",
                table: "AppRequests");

            

            migrationBuilder.AddColumn<decimal>(
                name: "EstOpIncome",
                table: "AppRequests",
                type: "decimal(18, 2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "LoanAmtRequest",
                table: "AppRequests",
                type: "decimal(18, 2)",
                nullable: false,
                defaultValue: 0m);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EstOpIncome",
                table: "AppRequests");

            migrationBuilder.DropColumn(
                name: "LoanAmtRequest",
                table: "AppRequests");

           

            migrationBuilder.AddColumn<string>(
                name: "NetOperatingIncome",
                table: "AppRequests",
                type: "varchar(10)",
                maxLength: 10,
                nullable: false,
                defaultValue: "");
        }
    }
}
