﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Appraiser.Data.Models.Response
{
    [DisplayName("Appraisal Response")]
    public class AppraisalResponseDTO
    {
        public int ValuationId { get; set; }
        public decimal ValuationAmount { get; set; }
        public int ReliabilityScore { get; set; }

        [MaxLength(45)]
        public string RecommendationText { get; set; }

        [MaxLength(45)]
        public string CredConfInterval { get; set; }

        [Required]
        public DateTime ValuationDate { get; set; }

        [Required]
        [MaxLength(10)]
        public string IsLevel1Sufficient { get; set; }

        public int StatusCode { get; set; }
        public string Status { get; set; }
    }
}
